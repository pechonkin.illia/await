#pragma once

#include <wheels/intrusive/forward_list.hpp>

#include <cassert>
#include <array>

namespace await::timers::wheel {

struct TimerMetadata {
  uint64_t timeout;
  uint64_t deadline;
};

// TimerNode - Node of intrusive forward list + descendant of TimerDeadLine

template <typename TimerNode>
class TimerWheel {
  static const size_t kWheelBits = 8;
  static const size_t kWheels = 64 / kWheelBits;
  static const size_t kWheelSize = 1 << kWheelBits;

  using Ticks = size_t;

 public:
  using TimerList = wheels::IntrusiveForwardList<TimerNode>;

 public:
  TimerWheel() {
    Init();
  }

  void Add(TimerNode* timer, Ticks delay) {
    timer->deadline = ticks_ + delay;

    size_t wheel_pos = timer->deadline;

    size_t wheel_index = 0;
    while (wheel_pos > kWheelSize) {
      wheel_pos /= kWheelSize;
      ++wheel_index;
    }

    wheels_[wheel_index][wheel_pos].PushBack(timer);
  }

  void Advance(Ticks ticks) {
    Advance(0, ticks);
    ticks_ += ticks;
  }

  TimerList TakeReadyTimers() {
    return std::move(ready_timers_);
  }

 private:
  void Advance(size_t wheel_index, Ticks steps) {
    if (steps > kWheelSize) {
      const size_t turns = steps / kWheelSize;
      Advance(wheel_index + 1, /*steps=*/turns);
      steps %= kWheelSize;
    }

    size_t wheel_pos = pos_[wheel_index];

    for (size_t i = 0; i < steps; ++i) {
      ++wheel_pos;

      if (wheel_pos == kWheelSize) {
        wheel_pos = 0;

        Advance(wheel_index + 1, /*steps=*/1);
      }

      TimerList& slot_timers = wheels_[wheel_index][wheel_pos];
      while (TimerNode* timer = slot_timers.PopFront()) {
        if (wheel_index > 0) {
          PropagateTimer(wheel_index, timer);
        } else {
          ready_timers_.PushBack(timer);
        }
      }
    }

    pos_[wheel_index] = wheel_pos;
  }

  void PropagateTimer(size_t wheel_index, TimerNode* timer) {
    assert(wheel_index > 0);
    --wheel_index;
    size_t wheel_timer_pos = GetWheelPos(timer->deadline, wheel_index);

    wheels_[wheel_index][wheel_timer_pos].PushBack(timer);
  }

  static size_t GetWheelPos(size_t time_point, size_t wheel_index) {
    return (time_point >> (kWheelBits * wheel_index)) & (kWheelSize - 1);
  }

  void Init() {
    for (size_t i = 0; i < kWheels; ++i) {
      pos_[i] = 0;
    }
    ticks_ = 0;
  }

 private:
  using Wheel = std::array<TimerList, kWheelSize>;

  std::array<Wheel, kWheels> wheels_;
  std::array<size_t, kWheels> pos_;
  Ticks ticks_;

  TimerList ready_timers_;
};

}  // namespace await::timers::wheel
