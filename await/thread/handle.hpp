#pragma once

#include <await/thread/fwd.hpp>
#include <await/fibers/core/fwd.hpp>

#include <await/tasks/core/hint.hpp>
#include <await/cancel/token.hpp>

namespace await::thread {

class Handle {
 public:
  Handle(IThread* thread)
      : thread_(thread) {
  }

  Handle()
      : Handle(nullptr) {
  }

  static Handle Invalid() {
    return {nullptr};
  }

  bool IsValid() const {
    return thread_ != nullptr;
  }

  void Resume(tasks::SchedulerHint hint = tasks::SchedulerHint::UpToYou);

  IThread* GetHandle() const {
    return Get();
  }

  cancel::Token CancelToken();

  // Fibers

  bool IsFiber() const;
  fibers::Fiber* GetFiber() const;

 private:
  IThread* Get() const;
  IThread* Release();

  void Validate() const;

 private:
  IThread* thread_;
};

}  // namespace await::thread
