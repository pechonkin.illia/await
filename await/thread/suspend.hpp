#pragma once

#include <await/thread/awaiter.hpp>

namespace await::thread {

void Suspend(IAwaiter& awaiter);

}  // namespace await::thread
