#include <await/thread/task_context.hpp>

#include <await/thread/this.hpp>

// Defaults
#include <carry/empty.hpp>
#include <await/cancel/never.hpp>

#include <carry/carrier.hpp>

namespace await::thread {

TaskContext::TaskContext(tasks::IExecutor* executor)
    : executor_(executor),
      user_context_(carry::Empty()),
      cancel_token_(cancel::Never()) {
}

TaskContext::TaskContext()
    : TaskContext(nullptr) {
}

void TaskContext::ResetContext() {
  executor_ = nullptr;
  user_context_ = carry::Empty();
  cancel_token_ = cancel::Never();
}

// Accessors

tasks::IExecutor& TaskContext::GetExecutor() const {
  WHEELS_ASSERT(executor_ != nullptr,
                "Cannot access current executor: I'm not a task");
  return *executor_;
}

}  // namespace await::thread
