#include <await/thread/this.hpp>

#include <await/thread/system.hpp>

#include <twist/ed/local/var.hpp>
#include <twist/ed/local/ptr.hpp>

#include <wheels/core/compiler.hpp>

namespace await::thread {

//////////////////////////////////////////////////////////////////////

static TWISTED_THREAD_LOCAL_VALUE(SystemThread, system_thread);

//////////////////////////////////////////////////////////////////////

static TWISTED_THREAD_LOCAL_PTR(IThread, this_thread);

//////////////////////////////////////////////////////////////////////

IThread* This() {
  IThread* t = this_thread;

  if (WHEELS_UNLIKELY(t == nullptr)) {
    t = &system_thread;
    this_thread = t;
  }

  return t;
}

//////////////////////////////////////////////////////////////////////

Scope::Scope(IThread* thread)
  : restore_(this_thread) {
  this_thread = thread;
}

Scope::~Scope() {
  this_thread = restore_;
}

//////////////////////////////////////////////////////////////////////

void Suspend(IAwaiter& awaiter) {
  this_thread->Suspend(&awaiter);
}

}  // namespace await::thread
