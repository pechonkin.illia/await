#pragma once

#include <twist/ed/stdlike/atomic.hpp>

#include <cstdint>

namespace await::thread::system {

class ParkingLot {
 public:
  using Epoch = uint32_t;

  Epoch Prepare();
  void ParkIf(Epoch old);
  void Wake();

 private:
  twist::ed::stdlike::atomic<Epoch> epoch_{0};
};

}  // namespace await::thread::system
