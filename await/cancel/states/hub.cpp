#include <await/cancel/states/hub.hpp>

namespace await::cancel {

namespace detail {

HubStateBase::~HubStateBase() {
  ReleaseHandlers();
}

void HubStateBase::Forward(Signal signal) {
  if (signal.CancelRequested()) {
    RequestCancel();
  } else {
    // Ignore?
  }

  ReleaseRef();
}

}  // namespace detail

}  // namespace await::cancel
