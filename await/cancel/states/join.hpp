#pragma once

#include <await/cancel/states/strand.hpp>

namespace await::cancel {

// Progress guarantee: Wait-freedom

namespace detail {

class JoinStateBase : public StrandStateBase {
 public:
  JoinStateBase(size_t inputs)
      : inputs_left_(inputs) {
  }

  // Wait-free
  void Forward(Signal) override;

 private:
  // Returns true if last one is closed
  bool CloseInput();

 private:
  twist::ed::stdlike::atomic<size_t> inputs_left_;
};

}  // namespace detail

}  // namespace await::cancel
