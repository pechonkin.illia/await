#pragma once

#include <await/thread/suspend.hpp>

#include <twist/ed/spin/lock.hpp>
#include <twist/ed/mutex/locker.hpp>

#include <twist/ed/stdlike/atomic.hpp>

#include <wheels/intrusive/list.hpp>

namespace await::fibers {

namespace detail {

// https://eli.thegreenplace.net/2018/basics-of-futexes/
// Wait queue bind to atomic memory cell

template <typename T>
class FutexLike {
 private:
  using WaitKey = size_t;

  using SpinLock = twist::ed::SpinLock;
  using UniqueLock = twist::ed::Locker<SpinLock>;

  class Waiter : public thread::ISuspendingAwaiter,
                 public wheels::IntrusiveListNode<Waiter> {
   public:
    Waiter(UniqueLock&& lock)
        : lock_(std::move(lock)) {
    }

    void AwaitSuspend(thread::Handle f) override {
      fiber_ = f;
      lock_.unlock();
    }

    void Resume() {
      fiber_.Resume();
    }

    thread::Handle Handle() {
      return fiber_;
    }

   private:
    UniqueLock lock_;
    thread::Handle fiber_;
  };

  using WaitList = wheels::IntrusiveList<Waiter>;

 public:
  ~FutexLike() {
    WHEELS_VERIFY(waiters_.IsEmpty(), "Waiters list is not empty");
  }

  WaitKey PrepareWait() {
    return epoch_.load();
  }

  // Park current fiber if value of atomic is equal to `old`
  void WaitIf(WaitKey epoch) {
    UniqueLock lock(spinlock_);

    if (epoch_.load() == epoch) {
      Waiter waiter(std::move(lock));
      waiters_.PushBack(&waiter);
      Suspend(waiter);
    }
  }

  bool NotifyOne() {
    WaitList to_wake;
    {
      UniqueLock lock(spinlock_);
      epoch_.store(epoch_.load() + 1);

      if (!waiters_.IsEmpty()) {
        to_wake.PushBack(waiters_.PopFront());
      }
    }

    return DoWake(std::move(to_wake)) > 0;
  }

  size_t NotifyAll() {
    WaitList to_wake;
    {
      UniqueLock lock(spinlock_);
      epoch_.store(epoch_.load() + 1);
      to_wake.Append(waiters_);  // Move
    }
    return DoWake(std::move(to_wake));
  }

 private:
  size_t DoWake(WaitList waiters) {
    size_t count = 0;
    while (!waiters.IsEmpty()) {
      // NB: Pop from intrusive list before Resume
      Waiter* waiter = waiters.PopFront();
      waiter->Resume();
      ++count;
    }
    return count;
  }

 private:
  SpinLock spinlock_;
  twist::ed::stdlike::atomic<size_t> epoch_{0};
  WaitList waiters_;
};

}  // namespace detail

}  // namespace await::fibers
