#pragma once

#include <await/thread/suspend.hpp>

#include <twist/ed/stdlike/atomic.hpp>

// std::{lock_guard, unique_lock}
#include <mutex>

namespace await::fibers {

class Mutex {
  using State = uintptr_t;

  struct States {
    enum _ {
      Unlocked = 0,
      LockedNoWaiters = 1,
      // + WaitNode address
    };
  };

  enum class AcquireStatus : bool {
    Acquired = true,
    Parked = false,
  };

  struct WaitNode {
    thread::Handle fiber;
    WaitNode* next{nullptr};
  };

  struct LockAwaiter : thread::IMaybeSuspendingAwaiter {
    explicit LockAwaiter(Mutex& mutex)
        : mutex_(mutex) {
    }
 
    thread::AwaitEither AwaitMaybeSuspend(thread::Handle h) override {
      waiter_.fiber = h;

      auto status = mutex_.AcquireOrPark(&waiter_);

      if (status == AcquireStatus::Acquired) {
        return thread::AwaitEither::Resume;
      } else {
        return thread::AwaitEither::Suspend;
      }
    }

    Mutex& mutex_;
    WaitNode waiter_;
  };

  struct UnlockAwaiter : thread::IAwaiter {
    explicit UnlockAwaiter(thread::Handle next_owner)
        : next_owner_(next_owner) {
    }

    // Symmetric Transfer
    thread::Handle AwaitSymmetricSuspend(thread::Handle curr_owner) override {
      // Copy next owner handle from the current owner's stack to the stack of the current worker
      thread::Handle next_owner =
          next_owner_;

      curr_owner.Resume();
      // <- This awaiter can be destroyed here

      return next_owner;
    }

    thread::Handle next_owner_;
  };

 public:
  void Lock() {
    if (TryAcquire()) {
      return;  // Fast path
    }
    LockAwaiter locker{*this};
    Suspend(locker);
  }

  bool TryLock() {
    return TryAcquire();
  }

  void Unlock() {
    Release();
  }

  // Lockable concept

  void lock() {
    Lock();
  }

  bool try_lock() {
    return TryLock();
  }

  void unlock() {
    return Unlock();
  }

 private:
  bool CasState(State from, State to, std::memory_order mo) {
    return state_.compare_exchange_strong(from, to, mo);
  }

  // Wait-free
  bool TryAcquire() {
    return CasState(States::Unlocked, States::LockedNoWaiters,
                    std::memory_order::acquire);
  }

  AcquireStatus AcquireOrPark(WaitNode* waiter) {
    while (true) {
      State state = state_.load();
      if (state == States::Unlocked) {
        if (TryAcquire()) {
          return AcquireStatus::Acquired;
        }
        continue;
      } else {
        if (state == States::LockedNoWaiters) {
          waiter->next = nullptr;
        } else {
          waiter->next = (WaitNode*)state;
        }
        if (CasState(state, (State)waiter, std::memory_order::release)) {
          return AcquireStatus::Parked;
        }
        continue;
      }
    }
  }

  void Release() {
    if (head_ != nullptr) {
      ResumeNextOwner(TakeNextOwner());
      return;
    } else {
      // head_ list is empty
      // state = LockNoWaiters | Waiters list
      while (true) {
        State state = state_.load();
        if (state == States::LockedNoWaiters) {
          if (CasState(States::LockedNoWaiters, States::Unlocked,
                       std::memory_order::release)) {
            return;
          }
          continue;
        } else {
          // Wait list
          WaitNode* waiters = (WaitNode*)state_.exchange(
              States::LockedNoWaiters, std::memory_order::acquire);
          head_ = Reverse(waiters);
          ResumeNextOwner(TakeNextOwner());
          return;
        }
      }
    }
  }

  static WaitNode* Reverse(WaitNode* head) {
    WaitNode* prev = nullptr;
    WaitNode* curr = head;

    while (curr != nullptr) {
      WaitNode* next = curr->next;
      curr->next = prev;
      prev = curr;
      curr = next;
    }

    return prev;
  }

  thread::Handle TakeNextOwner() {
    thread::Handle next_owner = head_->fiber;
    head_ = head_->next;
    return next_owner;
  }

  void ResumeNextOwner(thread::Handle f) {
    // f.Schedule();

    // f.Resume();

    UnlockAwaiter unlocker{f};
    Suspend(unlocker);
  }

 private:
  // Lock state + wait queue tail
  twist::ed::stdlike::atomic<State> state_{States::Unlocked};
  // Wait queue head
  WaitNode* head_{nullptr};
};

}  // namespace await::fibers
