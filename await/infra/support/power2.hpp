#pragma once

#include <cstdlib>

namespace await::support {

template <typename Uint>
inline constexpr bool IsPowerOf2(Uint value) {
  return (value > 0) && ((value & (value - 1)) == 0);
}

template <typename Uint>
constexpr Uint NextPowerOf2(Uint value, size_t pow = 1) {
  if ((value >> pow) == 0) {
    return (Uint)1 << pow;
  } else {
    return NextPowerOf2(value, pow + 1);
  }
}

}  // namespace await::support
