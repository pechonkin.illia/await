#pragma once

#include <await/tasks/core/task.hpp>

namespace await::tasks {

// Task scheduling algorithm
// (e.g. shared queue or work-stealing)

// See also IRunner

struct IScheduler {
  virtual ~IScheduler() = default;

  virtual bool StopRequested() const = 0;

  // Pick next task to run
  // Returns nullptr if stop requested
  virtual TaskBase* PickTask() = 0;
};

}  // namespace await::tasks
