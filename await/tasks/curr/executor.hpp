#pragma once

#include <await/tasks/core/executor.hpp>

namespace await::tasks::curr {

// Executor of the current task
IExecutor& Executor();

}  // namespace await::tasks::curr
