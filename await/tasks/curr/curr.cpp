#include <await/tasks/curr/context.hpp>
#include <await/tasks/curr/checkpoint.hpp>
#include <await/tasks/curr/executor.hpp>

#include <await/tasks/core/cancelled.hpp>

#include <await/thread/this.hpp>

namespace await::tasks::curr {

IExecutor& Executor() {
  return thread::This()->GetExecutor();
}

carry::Context Context() {
  return thread::This()->GetContext();
}

void Checkpoint() {
  if (thread::This()->CancelRequested()) {
    throw CancelledException{};
  }
}

}  // namespace await::tasks::curr
