#pragma once

#include <await/futures/impl/lazy/thunks/seq/flatten.hpp>

#include <await/futures/types/traits/value_of.hpp>

#include <await/futures/syntax/pipe.hpp>

namespace await::futures {

//////////////////////////////////////////////////////////////////////

namespace pipe {

struct [[nodiscard]] Flatten {
  template <SomeFuture Future>
  auto Pipe(Future input) {
    static_assert(SomeFuture<ValueOf<Future>>);
    return lazy::thunks::Flatten{std::move(input)};
  }
};

}  // namespace pipe

//////////////////////////////////////////////////////////////////////

inline auto Flatten() {
  return pipe::Flatten{};
}

}  // namespace await::futures
