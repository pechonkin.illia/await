#pragma once

#include <await/futures/impl/lazy/thunks/seq/input.hpp>

#include <await/futures/syntax/pipe.hpp>

namespace await::futures {

//////////////////////////////////////////////////////////////////////

namespace pipe {

struct [[nodiscard]] ToInput {
  size_t index;

  template <SomeFuture Future>
  auto Pipe(Future input) {
    return lazy::thunks::Input{std::move(input), index};
  }
};

}  // namespace pipe

//////////////////////////////////////////////////////////////////////

inline auto ToInput(size_t index = 0) {
  return pipe::ToInput{index};
}

}  // namespace await::futures
