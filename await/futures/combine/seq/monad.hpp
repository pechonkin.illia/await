#pragma once

// Map variations

#include <await/futures/combine/seq/map.hpp>
#include <await/futures/combine/seq/and_then.hpp>
#include <await/futures/combine/seq/or_else.hpp>
#include <await/futures/combine/seq/flat_map.hpp>
