#pragma once

#include <await/futures/impl/lazy/thunks/seq/status.hpp>

#include <await/futures/syntax/pipe.hpp>

namespace await::futures {

//////////////////////////////////////////////////////////////////////

namespace pipe {

struct [[nodiscard]] ToStatus {
  template <SomeFuture Future>
  futures::Future<bool> auto Pipe(Future input) {
    return lazy::thunks::ToStatus{std::move(input)};
  }
};

}  // namespace pipe

//////////////////////////////////////////////////////////////////////

inline auto ToStatus() {
  return pipe::ToStatus();
}

}  // namespace await::futures
