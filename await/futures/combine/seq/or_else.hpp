#pragma once

#include <await/futures/impl/lazy/thunks/seq/map.hpp>
#include <await/futures/impl/fun/unit.hpp>
#include <await/futures/impl/value/t/or_else.hpp>
#include <await/futures/impl/value/t/is_monad.hpp>

#include <await/futures/types/traits/value_of.hpp>

#include <await/futures/syntax/pipe.hpp>

namespace await::futures {

//////////////////////////////////////////////////////////////////////

namespace pipe {

template <typename Handler>
struct [[nodiscard]] OrElse {
  Handler handler;

  OrElse(Handler h)
      : handler(std::move(h)) {
  }

  template <SomeFuture InputFuture>
  auto Mapper() {
    return futures::value::apply::OrElse<typename InputFuture::ValueType, Handler>{std::move(handler)};
  }

  template <SomeFuture InputFuture>
  Future<ValueOf<InputFuture>> auto Pipe(InputFuture input) {
    static_assert(value::IsMonad<ValueOf<InputFuture>>);

    return lazy::thunks::Map{std::move(input), Mapper<InputFuture>()};
  }
};

}  // namespace pipe

//////////////////////////////////////////////////////////////////////

template <typename Handler>
auto OrElse(Handler handler) {
  return pipe::OrElse{fun::Unitize(std::move(handler))};
}

}  // namespace await::futures
