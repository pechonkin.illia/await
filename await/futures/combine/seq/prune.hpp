#pragma once

#include <await/futures/types/future.hpp>

#include <await/futures/types/traits/value_of.hpp>

#include <await/futures/impl/lazy/thunks/seq/prune.hpp>

namespace await::futures {

//////////////////////////////////////////////////////////////////////

namespace pipe {

struct [[nodiscard]] Prune {
  template <SomeFuture InputFuture>
  Future<ValueOf<InputFuture>> auto Pipe(InputFuture input) {
    return lazy::thunks::Pruned<ValueOf<InputFuture>>{std::move(input)};
  }
};

}  // namespace pipe

//////////////////////////////////////////////////////////////////////

// Do not inline thunk, free memory on completion

inline auto Prune() {
  return pipe::Prune{};
}

inline auto NoInline() {
  return pipe::Prune{};
}

}  // namespace await::futures
