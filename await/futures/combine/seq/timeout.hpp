#pragma once

#include <await/futures/make/after.hpp>
#include <await/futures/make/value.hpp>

#include <await/futures/combine/seq/sequence.hpp>
#include <await/futures/combine/par/interrupt.hpp>

#include <await/futures/impl/value/t/timeout.hpp>

#include <await/futures/syntax/pipe.hpp>

namespace await::futures {

//////////////////////////////////////////////////////////////////////

namespace pipe {

struct [[nodiscard]] WithTimeout {
  timers::Delay delay;

  template <typename T>
  static auto Timeout() {
    return value::WithTimeoutTraits<T>::Timeout();
  }

  template <SomeFuture Future>
  SomeFuture auto Pipe(Future input) {
    using T = typename Future::ValueType;

    auto timeout = futures::Sequence(
        futures::After(delay),
        futures::Value(Timeout<T>()));

    return futures::Interrupt(std::move(input), std::move(timeout));
  }
};

}  // namespace pipe

//////////////////////////////////////////////////////////////////////

inline auto WithTimeout(timers::Delay delay) {
  return pipe::WithTimeout{delay};
}

}  // namespace await::futures
