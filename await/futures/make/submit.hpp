#pragma once

#include <await/futures/types/future.hpp>

#include <await/futures/make/just.hpp>

#include <await/futures/combine/seq/via.hpp>
#include <await/futures/combine/seq/with.hpp>
#include <await/futures/combine/seq/apply.hpp>

#include <await/tasks/exe/inline.hpp>

// Current
#include <await/tasks/curr/context.hpp>
#include <await/tasks/curr/executor.hpp>

namespace await::futures {

//////////////////////////////////////////////////////////////////////

/*
 * Executes `fun` via executor `exe`
 *
 * Shortcut for
 *  Just()
 *    | Via(where)
 *    | With(tasks::curr::Context())
 *    | Apply([fun](Unit) { return fun(); })
 *
 * Example:
 *
 * auto task = futures::Submit(pool, []() -> int {
 *   return 42;
 * });
 *
 * auto value = Await(std::move(task));
 */

template <typename F>
SomeFuture auto Submit(tasks::IExecutor& exe, F fun) {
  // clang-format off

  return Just()
    | Via(exe)
    | With(tasks::curr::Context())
    | Apply(std::move(fun));

  // clang-format on
}

//////////////////////////////////////////////////////////////////////

/*
 * Executes `fun` via the current executor
 *
 * Shortcut for Execute(tasks::curr::Executor(), fun)
 */

template <typename F>
SomeFuture auto Spawn(F fun) {
  return Submit(tasks::curr::Executor(), std::move(fun));
}

//////////////////////////////////////////////////////////////////////

/*
 * Executes `fun` via `Inline` executor
 */

template <typename F>
SomeFuture auto Invoke(F fun) {
  return Submit(tasks::Inline(), std::move(fun));
}

}  // namespace await::futures
