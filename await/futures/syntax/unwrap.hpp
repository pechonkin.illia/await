#pragma once

#include <await/futures/run/unwrap.hpp>

/*
 * "Unwrap" operator (*)
 *
 * Unwrap ready (synchronous) future without waiting
 * ~ operator * for std::optional
 *
 * Synonym for | futures::Unwrap()
 *
 */

template <await::futures::SomeFuture Future>
auto operator *(Future f) {
  return std::move(f) | await::futures::Unwrap();
}
