#pragma once

#include <await/futures/combine/seq/start.hpp>

#include <await/futures/types/future.hpp>

/*
 * "Bang" operator (!)
 *
 * Named after bang patterns in Strict Haskell
 * https://www.fpcomplete.com/haskell/tutorial/all-about-strictness/
 *
 * Turns lazy Future into EagerFuture / starts operation
 *
 */

template <await::futures::SomeFuture Future>
auto operator !(Future f) {
  return std::move(f) | await::futures::Start();
}
