#pragma once

#include <await/futures/model/thunk.hpp>

#include <await/futures/impl/consumers/tagged.hpp>

namespace await::futures::lazy {

namespace thunks {

struct FlattenTags {
  struct Future {};
  struct Value {};
};

template <Thunk Producer>
class [[nodiscard]] Flatten final
    : private futures::consumers::ITaggedConsumer<typename Producer::ValueType, FlattenTags::Future>,
      private futures::consumers::ITaggedConsumer<typename Producer::ValueType::ValueType, FlattenTags::Value> {
 public:
  using FutureType = typename Producer::ValueType;
  static_assert(SomeFuture<FutureType>);

  using ValueType = typename FutureType::ValueType;

 public:
  Flatten(Producer&& thunk)
      : producer_(std::move(thunk)) {
  }

  // Non-copyable
  Flatten(const Flatten&) = delete;

  // Movable
  Flatten(Flatten&&) = default;

  // Lazy protocol

  void Start(IConsumer<ValueType>* consumer) {
    consumer_ = consumer;
    producer_.Start(AsFutureConsumer());
  }

  // Statement

  void IAmLazy() {
    // No-op
  }

 private:
  // IConsumer<FutureType>

  futures::consumers::ITaggedConsumer<FutureType, FlattenTags::Future>* AsFutureConsumer() {
    return this;
  }

  void ConsumeTagged(Output<FutureType> input, FlattenTags::Future) noexcept override {
    future_.emplace(std::move(input.value));

    std::move(*future_).Start(AsValueConsumer());
  }

  void CancelTagged(Context ctx, FlattenTags::Future) noexcept override {
    consumer_->Cancel(std::move(ctx));
  }

  // IConsumer<ValueType>

  futures::consumers::ITaggedConsumer<ValueType, FlattenTags::Value>* AsValueConsumer() {
    return this;
  }

  void ConsumeTagged(Output<ValueType> output, FlattenTags::Value) noexcept override {
    // Reset scheduler hint
    output.context.exe.hint = tasks::SchedulerHint::UpToYou;
    consumer_->Consume(std::move(output));
  }

  void CancelTagged(Context ctx, FlattenTags::Value) noexcept override {
    consumer_->Cancel(std::move(ctx));
  }

  // Cancellation

  cancel::Token CancelToken() override {
    return consumer_->CancelToken();
  }

 private:
  Producer producer_;
  std::optional<FutureType> future_;

  IConsumer<ValueType>* consumer_;
};

}  // namespace thunks

}  // namespace await::futures::lazy
