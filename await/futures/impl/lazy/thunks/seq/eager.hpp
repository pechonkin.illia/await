#pragma once

#include <await/futures/model/thunk.hpp>

#include <await/futures/impl/eager/state.hpp>

namespace await::futures::lazy {

namespace thunks {

template <typename V>
class [[nodiscard]] Eager final : private IConsumer<V> {
 public:
  using ValueType = V;

 public:
  Eager(eager::StateRef<V> state)
      : state_(std::move(state)) {
  }

  ~Eager() {
    if (state_.IsValid()) {
      Drop();
    }
  }

  // Non-copyable
  Eager(const Eager&) = delete;

  // Movable
  Eager(Eager&&) = default;

  // Lazy protocol

  void Start(IConsumer<V>* consumer) {
    ReleaseState()->SetConsumer(consumer);
  }

  // Internal

  eager::StateRef<V> ReleaseState() {
    assert(state_.IsValid());
    return std::move(state_);
  }

  // Manual cancellation

  void RequestCancel() {
    ReleaseState()->Drop();
  }

  // Polling

  bool IsValid() const {
    return state_.IsValid();
  }

  bool HasOutput() const {
    return state_->GetReadyState() == race::Ready::HasOutput;
  }

  bool IsCancelled() const {
    return state_->GetReadyState() == race::Ready::Cancelled;
  }

  // Statement

  void IAmEager() {
    // No-op
  }

  void Drop() {
    ReleaseState()->Drop();
  }

 private:
  // IConsumer<V>

  cancel::Token CancelToken() override {
    return consumer_->CancelToken();
  }

  void Consume(Output<V> output) noexcept override {
    consumer_->Consume(std::move(output));
  }

  void Cancel(Context ctx) noexcept override {
    consumer_->Cancel(std::move(ctx));
  }

 private:
  eager::StateRef<V> state_;

  IConsumer<V>* consumer_{nullptr};
};

}  // namespace thunks

}  // namespace await::futures::lazy

