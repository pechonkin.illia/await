#pragma once

#include <await/futures/model/thunk.hpp>

#include <await/tasks/core/cancelled.hpp>

#include <await/thread/this.hpp>

#include <wheels/core/defer.hpp>
#include <wheels/core/exception.hpp>

#include <cassert>
#include <optional>

namespace await::futures::lazy {

namespace thunks {

// [Await.Task(Manual,User)]
template <Thunk Producer, typename Mapper>
class [[nodiscard]] Map final : private IConsumer<typename Producer::ValueType>,
                                private tasks::TaskBase {
 public:
  using InputValueType = typename Producer::ValueType;
  using OutputValueType = std::invoke_result_t<Mapper, InputValueType>;

  using ValueType = OutputValueType;

 public:
  Map(Producer&& parent, Mapper&& mapper)
      : producer_(std::move(parent)),
        mapper_(std::move(mapper)) {
  }

  // Non-copyable
  Map(const Map&) = delete;

  // Movable
  Map(Map&&) = default;

  // Lazy protocol

  void Start(IConsumer<OutputValueType>* consumer) {
    consumer_ = consumer;
    producer_.Start(/*consumer=*/this);
  }

 private:
  // IConsumer<InputValueType>

  // Propagate
  cancel::Token CancelToken() override {
    return consumer_->CancelToken();
  }

  void Consume(Output<InputValueType> input) noexcept override {
    if (CancelRequested()) {
      Cancel(std::move(input.context));
    } else {
      input_ = std::move(input);
      input_->context.exe.executor->Submit(/*task=*/this, input_->context.exe.hint);
    }
  }

  void Cancel(Context ctx) noexcept override {
    consumer_->Cancel(std::move(ctx));  // Propagate instantly
  }

  // tasks::ITask

  void Run() noexcept override {
    try {
      auto item = RunMapper();
      // Happy path
      consumer_->Consume({std::move(item), OutputContext(std::move(input_->context))});
    } catch (tasks::CancelledException) {
      // Cancellation
      consumer_->Cancel(std::move(input_->context));
    }
  }

 private:
  static Context OutputContext(Context input) {
    input.exe.hint = tasks::SchedulerHint::Next;
    return input;
  }

  bool CancelRequested() const {
    return consumer_->CancelToken().CancelRequested();
  }

  void SetupContext(thread::TaskContext* carrier) {
    carrier->SetExecutor(input_->context.exe.executor);
    carrier->SetUserContext(input_->context.user);
    carrier->SetCancelToken(consumer_->CancelToken());
  }

  OutputValueType RunMapper() {
    auto* carrier = thread::This();

    SetupContext(carrier);

    // Reset context after invocation
    // NB: Before consumer->Cancel/Consume!
    wheels::Defer cleanup([carrier]() {
      carrier->ResetContext();
    });

    assert(!CancelToken().HasLinks());

    {
      // NB: Destroy mapper after invocation!
      auto mapper = std::move(mapper_);

      return mapper(std::move(input_->value));
    }
  }

 private:
  Producer producer_;

  std::optional<Output<InputValueType>> input_;
  Mapper mapper_;

  IConsumer<OutputValueType>* consumer_{nullptr};
};

}  // namespace thunks

}  // namespace await::futures::lazy
