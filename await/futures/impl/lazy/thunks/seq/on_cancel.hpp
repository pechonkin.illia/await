#pragma once

#include <await/futures/model/thunk.hpp>

#include <await/tasks/curr/executor.hpp>

#include <await/thread/this.hpp>

#include <await/cancel/never.hpp>

#include <wheels/core/defer.hpp>
#include <wheels/core/panic.hpp>

namespace await::futures::lazy {

namespace thunks {

// [Await.Task(Manual,User)]
template <Thunk Observed, typename F>
class [[nodiscard]] OnCancel final
    : private IConsumer<typename Observed::ValueType>,
      private tasks::TaskBase {
 public:
  using ValueType = typename Observed::ValueType;

 public:
  OnCancel(Observed&& thunk, F&& handler)
      : observed_(std::move(thunk)),
        handler_(std::move(handler)) {
    //
  }

  // Non-copyable
  OnCancel(const OnCancel&) = delete;

  // Movable
  OnCancel(OnCancel&&) = default;

  ~OnCancel() {
    //
  }

  // Lazy protocol

  void Start(IConsumer<ValueType>* consumer) {
    consumer_ = consumer;
    observed_.Start(/*consumer=*/this);
  }

 private:
  // IConsumer<ValueType>

  // Propagate
  cancel::Token CancelToken() override {
    return consumer_->CancelToken();
  }

  void Consume(Output<ValueType> output) noexcept override {
    // Propagate
    consumer_->Consume(std::move(output));
  }

  void Cancel(Context ctx) noexcept override {
    context_.emplace(std::move(ctx));
    context_->exe.executor->Submit(this, tasks::SchedulerHint::Next);
  }

  // ITask

  void Run() noexcept override {
    RunCancelHandler();
    consumer_->Cancel(std::move(*context_));  // Propagate cancel
  }

  void SetupContext(thread::TaskContext* carrier) {
    carrier->SetExecutor(context_->exe.executor);
    carrier->SetUserContext(context_->user);
    // Disable cancellation for cancel handler
    carrier->SetCancelToken(cancel::Never());
  }

  void RunCancelHandler() {
    auto* carrier = thread::This();

    SetupContext(carrier);

    wheels::Defer cleanup([carrier]() {
      carrier->ResetContext();
    });

    try {
      handler_();
    } catch (...) {
      WHEELS_PANIC("Unhandled exception in Future cancel handler");
    }
  }

 private:
  Observed observed_;
  F handler_;

  std::optional<Context> context_;

  IConsumer<ValueType>* consumer_;
};

}  // namespace thunks

}  // namespace await::futures::lazy
