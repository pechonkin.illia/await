#pragma once

#include <await/futures/model/thunk.hpp>

#include <wheels/core/unit.hpp>

namespace await::futures::lazy {

namespace thunks {

class [[nodiscard]] JustUnit {
 public:
  using ValueType = wheels::Unit;

 public:
  JustUnit() = default;

  // Non-copyable
  JustUnit(const JustUnit&) = delete;

  // Movable
  JustUnit(JustUnit&&) = default;

  // Lazy protocol

  void Start(IConsumer<wheels::Unit>* consumer) {
    consumer->Consume(wheels::Unit{});
  }
};

}  // namespace thunks

}  // namespace await::futures::lazy
