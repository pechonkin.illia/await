#pragma once

#include <await/futures/model/consumer.hpp>

#include <await/cancel/never.hpp>

#include <wheels/core/assert.hpp>

#include <optional>

namespace await::futures::lazy {

namespace terminators {

template <typename T>
class UnwrapConsumer : public IConsumer<T> {
 public:
  void Consume(Output<T> output) noexcept override {
    value_.emplace(std::move(output.value));
  }

  void Cancel(Context) noexcept override {
    WHEELS_PANIC("Failed to consume output: Cancelled");
  }

  T ExpectValue() {
    WHEELS_VERIFY(value_, "Failed to consume output: Not ready");
    return std::move(*value_);
  }

  cancel::Token CancelToken() override {
    return cancel::Never();
  }

 private:
  std::optional<T> value_;
};

}  // namespace terminators

}  // namespace await::futures::lazy
