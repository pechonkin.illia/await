#pragma once

#include <optional>

namespace await::futures::value {

namespace match {

template <typename T>
struct IsStdOptional {
  static const bool Value = false;
};

template <typename T>
struct IsStdOptional<std::optional<T>> {
  static const bool Value = true;
};

}  // namespace match

template <typename T>
concept IsStdOptional = match::IsStdOptional<T>::Value;

}  // namespace await::futures::value
