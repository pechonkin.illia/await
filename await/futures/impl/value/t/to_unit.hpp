#pragma once

#include <wheels/core/unit.hpp>

namespace await::futures::value {

template <typename T>
struct ToUnitTraits {
  using UnitType = wheels::Unit;

  static wheels::Unit ToUnit(T) {
    return {};
  }
};

}  // namespace await::futures::value
