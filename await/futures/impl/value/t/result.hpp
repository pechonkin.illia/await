#pragma once

namespace await::futures::value {

template <typename InputType>
struct ResultTraits {
  static bool IsOk(const InputType&) {
    return true;
  }
};

}  // namespace await::futures::value
