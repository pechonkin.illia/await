#pragma once

namespace await::futures::value {

namespace apply {

template <typename T, typename F>
struct Map {
  F f;

  auto operator()(T v) {
    return f(std::move(v));
  }
};

}  // namespace apply

}  // namespace await::futures::value
