#pragma once

#include <await/futures/impl/value/t/timeout.hpp>

#include <tl/expected.hpp>

#include <system_error>

namespace await::futures::value {

//////////////////////////////////////////////////////////////////////

template <typename E>
struct TimeoutTraits {
  // Missing
};

template <>
struct TimeoutTraits<std::error_code> {
  static std::error_code Timeout() {
    return std::make_error_code(std::errc::timed_out);
  }
};

//////////////////////////////////////////////////////////////////////

template <typename T, typename E>
struct WithTimeoutTraits<tl::expected<T, E>> {
  using ExpectedType = tl::expected<T, E>;

  static ExpectedType Timeout() {
    return tl::unexpected(TimeoutTraits<E>::Timeout());
  }
};

}  // namespace await::futures::value
