#pragma once

#include <await/futures/impl/value/t/is_monad.hpp>

#include <tl/expected.hpp>

namespace await::futures::value {

namespace match {

template <typename T, typename E>
struct IsMonad<tl::expected<T, E>> : std::true_type {};

}  // namespace match

}  // namespace await::futures::value
