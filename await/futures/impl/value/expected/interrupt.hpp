#pragma once

#include <await/futures/impl/value/t/interrupt.hpp>

#include <tl/expected.hpp>

namespace await::futures::value {

// expected<T, E>, E -> expected<T, E>

template <typename T, typename E>
struct InterruptTraits<tl::expected<T, E>, E> {
  using Result = tl::expected<T, E>;

  using ValueType = Result;

  static Result Value(Result r) {
    return r;
  }

  static Result Interrupt(E e) {
    return tl::unexpected(std::move(e));
  }
};

}  // namespace await::futures::value
