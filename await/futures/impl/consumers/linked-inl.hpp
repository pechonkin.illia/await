
namespace await::futures::consumers {

template <typename T>
LinkedConsumer<T>::LinkedConsumer(IConsumer<T>* consumer, cancel::Source&& source)
    : consumer_(consumer),
      link_(consumer->CancelToken().Link(std::move(source))) {
}

template <typename T>
LinkedConsumer<T>::LinkedConsumer(LinkedConsumer<T>&& that)
  : consumer_(std::exchange(that.consumer_, nullptr)),
    link_(std::move(that.link_)) {
}

template <typename T>
LinkedConsumer<T>& LinkedConsumer<T>::operator=(LinkedConsumer<T>&& that) {
  assert(!IsValid());

  consumer_ = std::exchange(that.consumer_, nullptr);
  link_ = std::move(that.link_);

  return *this;
}

template <typename T>
IConsumer<T>* LinkedConsumer<T>::ReleaseConsumer() {
  assert(IsValid());

  link_.Reset();
  return std::exchange(consumer_, nullptr);
}

}  // namespace await::futures::consumers
