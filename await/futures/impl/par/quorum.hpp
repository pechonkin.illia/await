#pragma once

#include <await/futures/impl/value/t/all.hpp>

#include <await/futures/impl/par/detail/maybe.hpp>

#include <twist/ed/spin/lock.hpp>

#include <cassert>
#include <optional>
#include <vector>
#include <utility>

namespace await::futures {

namespace combinators {

template <typename InputType>
class QuorumCombinator {
 public:
  using Traits = value::AllTraits<InputType>;

  using VectorType = typename Traits::VectorType;
  using OutputType = VectorType;

 public:
  QuorumCombinator(size_t inputs, size_t threshold)
      : inputs_(inputs),
        threshold_(threshold) {
  }

  Maybe<OutputType> Combine(InputType input, CombinatorContext) {
    if (Traits::IsOk(input)) {
      return CombineValue(std::move(input));
    } else {
      return CombineError(std::move(input));
    }
  }

  Maybe<OutputType> CombineValue(InputType value) {
    std::lock_guard locker(mutex_);

    if (completed_) {
      return {};  // Already
    }

    values_.push_back(Traits::UnwrapValue(std::move(value)));

    if (values_.size() == threshold_) {
      // Quorum reached
      completed_ = true;
      return Traits::WrapValues(std::move(values_));
    }

    return {};
  }

  // Returns true on failure
  Maybe<OutputType> CombineError(InputType error) {
    std::lock_guard locker(mutex_);

    if (completed_) {
      return {};  // Already
    }

    ++errors_;
    if (ImpossibleToReachQuorum()) {
      completed_ = true;
      return Traits::PropagateError(std::move(error));
    }

    return {};
  }

  bool CombineCancel() {
    std::lock_guard locker(mutex_);

    return !std::exchange(completed_, true);
  }

  bool IsCompleted() const {
    std::lock_guard locker(mutex_);

    return completed_;
  }

 private:
  bool ImpossibleToReachQuorum() const {
    return errors_ + threshold_ > inputs_;
  }

 private:
  const size_t inputs_;
  const size_t threshold_;

  mutable twist::ed::SpinLock mutex_;
  VectorType values_;
  size_t errors_{0};

  bool completed_{false};
};

}  // namespace combinators

}  // namespace await::futures
