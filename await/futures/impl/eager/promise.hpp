#pragma once

#include <await/futures/impl/eager/states/contract.hpp>

#include <await/cancel/detail/handler.hpp>

namespace await::futures::eager {

//////////////////////////////////////////////////////////////////////

template <typename T>
class Promise {
  using State = states::Contract<T>;
  using StateRef = refer::Ref<State>;

 public:
  // Non-copyable
  Promise(const Promise<T>&) = delete;
  Promise& operator=(const Promise<T>&) = delete;

  // Movable
  Promise(Promise<T>&&) = default;
  Promise& operator=(Promise<T>&&) = default;

  bool IsValid() const {
    return state_.IsValid();
  }

  void Set(T value) && {
    std::move(*this).Set(ToOutput(std::move(value)));
  }

  void Set(Output<T> output) && {
    ReleaseState()->SetOutput(std::move(output));
  }

  // Cancel signal

  bool CancelRequested() const {
    return state_->CancelRequested();
  }

  cancel::Token CancelToken() {
    return cancel::Token::FromState(state_);
  }

  template <typename F>
  void CancelSubscribe(tasks::IExecutor& executor, F handler) {
    auto handler_state = refer::New<cancel::detail::Handler<F>>(
        std::move(handler), executor);

    state_->AddHandler(handler_state.Release());
  }

  void Detach() && {
    state_.Reset();
  }

  // Cancellation

  void Cancel() && {
    ReleaseState()->SetCancel(Context{});
  }

  ~Promise() {
    if (IsValid()) {
      if (CancelRequested()) {
        state_->SetCancel(Context{});
      } else {
        // Or panics?
        state_.Reset();
      }
    }
  }

  // Private!
  explicit Promise(StateRef state)
      : state_(std::move(state)) {
  }

 private:
  Output<T> ToOutput(T value) {
    return {std::move(value), {}};
  }

  StateRef ReleaseState() {
    WHEELS_ASSERT(state_.IsValid(), "Promise is invalid");
    return std::move(state_);
  }

 private:
  StateRef state_;
};

}  // namespace await::futures::eager
