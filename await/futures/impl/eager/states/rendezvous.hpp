#pragma once

#include <await/futures/impl/eager/state.hpp>

#include <await/futures/impl/race/rendezvous.hpp>

#include <optional>

namespace await::futures::eager {

namespace states {

//////////////////////////////////////////////////////////////////////

// [Await.FutureState]
template <typename T, class CancelStateBase>
class RendezvousStateBase : public IState<T>,
                            public CancelStateBase {
 protected:
  using CancelBase = CancelStateBase;

 public:
  RendezvousStateBase() {
  }

  // Subscription

  void SetConsumer(IConsumer<T>* consumer) override {
    rendezvous_.SetConsumer(consumers::Link(consumer, CancelSource()));
  }

  void Drop() override {
    CancelStateBase::RequestCancel();
  }

  // Polling

  race::Ready GetReadyState() const override {
    return rendezvous_.GetState();
  }

  // Cancellation

  cancel::Source CancelSource() {
    return cancel::Source::FromState(this);
  }

  cancel::Token CancelToken() {
    return cancel::Token::FromState(this);
  }

  // For Promise

  void SetOutput(Output<T> output) {
    rendezvous_.SetOutput(std::move(output));
  }

  void SetCancel(Context ctx) {
    rendezvous_.SetCancel(std::move(ctx));
  }

 private:
  race::Rendezvous<T> rendezvous_;
};

}  // namespace states

}  // namespace await::futures::eager
