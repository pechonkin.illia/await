#pragma once

#include <await/futures/model/thunk.hpp>

#include <await/futures/impl/consumers/linked.hpp>
#include <await/futures/impl/eager/state.hpp>
#include <await/futures/impl/race/rendezvous.hpp>

#include <await/cancel/states/strand.hpp>

#include <refer/ref_counted.hpp>

namespace await::futures::eager {

namespace states {

// [Await.FutureState]
template <lazy::Thunk Thunk>
class Evaluate final : public IState<typename Thunk::ValueType>,
                       public IConsumer<typename Thunk::ValueType>,
                       public cancel::detail::StrandStateBase,
                       public refer::RefCounted<Evaluate<Thunk>> {
  using ValueType = typename Thunk::ValueType;

 public:
  Evaluate(Thunk&& thunk)
      : thunk_(std::move(thunk)) {
  }

  // One-shot
  void StartEvaluation() {
    thunk_.Start(AsConsumer());
  }

  // IFutureState

  // Consume

  void SetConsumer(IConsumer<ValueType>* consumer) override {
    rendezvous_.SetConsumer(consumers::Link(consumer, CancelSource()));
  }

  void Drop() override {
    RequestCancel();
  }

  // Polling

  race::Ready GetReadyState() const override {
    return rendezvous_.GetState();
  }

 private:
  cancel::Source CancelSource() {
    return cancel::Source::FromState(this);
  }

  // IConsumer<ValueType>

  cancel::Token CancelToken() override {
    return cancel::Token::FromState(this);
  }

  void Consume(Output<ValueType> output) noexcept override {
    rendezvous_.SetOutput(std::move(output));
    CompleteConsume();
  }

  void Cancel(Context ctx) noexcept override {
    rendezvous_.SetCancel(std::move(ctx));
    CompleteConsume();
  }

  // Memory management

  IConsumer<ValueType>* AsConsumer() {
    this->AddRef();
    return this;
  }

  void CompleteConsume() {
    this->ReleaseRef();  // Memory management
  }

 private:
  Thunk thunk_;
  race::Rendezvous<ValueType> rendezvous_;
};

}  // namespace states

}  // namespace await::futures::eager
