#include <await/futures/values/optional.hpp>

#include <await/futures/make/submit.hpp>
#include <await/futures/make/after.hpp>

#include <await/futures/make/contract.hpp>

#include <await/futures/combine/par/all.hpp>
#include <await/futures/combine/par/join.hpp>
#include <await/futures/combine/par/first_of.hpp>
#include <await/futures/combine/par/quorum.hpp>
#include <await/futures/combine/par/interrupt.hpp>

#include <await/futures/combine/seq/map.hpp>

#include <await/futures/run/unwrap.hpp>
#include <await/futures/run/go.hpp>

#include <await/futures/syntax/unwrap.hpp>
#include <await/futures/syntax/bang.hpp>
#include <await/futures/syntax/go.hpp>

#include <await/await.hpp>

#include <await/tasks/exe/manual.hpp>

#include <await/timers/impl/queue.hpp>

#include <wheels/test/framework.hpp>

using namespace await;

//////////////////////////////////////////////////////////////////////

struct MoveOnly {
  explicit MoveOnly(std::string data_) : data(data_) {
  }

  MoveOnly(const MoveOnly& that) = delete;
  MoveOnly& operator=(const MoveOnly& that) = delete;

  MoveOnly(MoveOnly&& that) = default;

  std::string data;
};

struct NonDefaultConstructable {
  explicit NonDefaultConstructable(int v) : value(v) {
  }

  int value{0};
};

//////////////////////////////////////////////////////////////////////

TEST_SUITE(FuturePar) {
  SIMPLE_TEST(All1) {
    tasks::ManualExecutor manual;

    auto l1 = futures::Submit(manual, []() {
      return 3;
    });

    auto l2 = futures::Submit(manual, []() {
      return 2;
    });

    auto l3 = futures::Submit(manual, []() {
      return 1;
    });

    auto all = !futures::All(std::move(l1), std::move(l2), std::move(l3));

    ASSERT_FALSE(all.HasOutput());

    manual.RunAtMost(2);

    // Still not completed
    ASSERT_FALSE(all.HasOutput());

    manual.Drain();

    ASSERT_TRUE(all.HasOutput());

    auto output = *std::move(all);
    ASSERT_EQ(output, std::vector<int>({3, 2, 1}));
  }

  SIMPLE_TEST(All2) {
    using OptInt = std::optional<int>;

    auto [f1, p1] = futures::Contract<OptInt>();
    auto [f2, p2] = futures::Contract<OptInt>();

    auto all = !futures::All(std::move(f1), std::move(f2));

    ASSERT_FALSE(all.HasOutput());

    // First error
    std::move(p2).Set(std::nullopt);

    ASSERT_TRUE(all.HasOutput());

    // Second error
    std::move(p1).Set(std::nullopt);

    ASSERT_TRUE(all.HasOutput());

    auto output = *std::move(all);
    ASSERT_FALSE(output);
  }

  SIMPLE_TEST(AllNonDefaultConstructable) {
    auto [f1, p1] = futures::Contract<NonDefaultConstructable>();
    auto [f2, p2] = futures::Contract<NonDefaultConstructable>();

    auto all = !futures::All(std::move(f1), std::move(f2));

    ASSERT_FALSE(all.HasOutput());

    std::move(p1).Set(NonDefaultConstructable{1});

    ASSERT_FALSE(all.HasOutput());

    std::move(p2).Set(NonDefaultConstructable{2});

    ASSERT_TRUE(all.HasOutput());

    auto output = *std::move(all);

    ASSERT_EQ(output[0].value, 1);
    ASSERT_EQ(output[1].value, 2);
  }

  SIMPLE_TEST(AllEmptyInput) {
    std::vector<futures::BoxedFuture<int>> inputs;
    auto all = !futures::All(std::move(inputs));

    ASSERT_TRUE(all.HasOutput());
  }

  SIMPLE_TEST(FirstOf1) {
    using OptInt = std::optional<int>;

    auto [f1, p1] = futures::Contract<OptInt>();
    auto [f2, p2] = futures::Contract<OptInt>();

    auto first_of = !futures::FirstOf(std::move(f1), std::move(f2));

    ASSERT_FALSE(first_of.HasOutput());

    std::move(p2).Set(13);

    ASSERT_TRUE(first_of.HasOutput());

    std::move(p1).Set(std::nullopt);

    auto output = *std::move(first_of);

    ASSERT_EQ(output, 13);
  }

  SIMPLE_TEST(FirstOf2) {
    using OptInt = std::optional<int>;

    auto [f1, p1] = futures::Contract<OptInt>();
    auto [f2, p2] = futures::Contract<OptInt>();
    auto [f3, p3] = futures::Contract<OptInt>();

    auto first_of = !futures::FirstOf(std::move(f1), std::move(f2), std::move(f3));

    ASSERT_FALSE(first_of.HasOutput());

    std::move(p1).Set(std::nullopt);

    ASSERT_FALSE(first_of.HasOutput());

    std::move(p3).Set(std::nullopt);

    ASSERT_FALSE(first_of.HasOutput());

    std::move(p2).Set(44);

    ASSERT_TRUE(first_of.HasOutput());

    auto output = *std::move(first_of);

    ASSERT_EQ(output, 44);
  }

  SIMPLE_TEST(FirstOfNonDefaultConstructable) {
    auto [f1, p1] = futures::Contract<NonDefaultConstructable>();
    auto [f2, p2] = futures::Contract<NonDefaultConstructable>();

    auto first_of = !futures::FirstOf(std::move(f1), std::move(f2));

    ASSERT_FALSE(first_of.HasOutput());

    std::move(p2).Set(NonDefaultConstructable{2});

    ASSERT_TRUE(first_of.HasOutput());

    std::move(p1).Set(NonDefaultConstructable{1});

    ASSERT_TRUE(first_of.HasOutput());

    auto output = *std::move(first_of);
    ASSERT_EQ(output.value, 2);
  }

  SIMPLE_TEST(Join) {
    auto [f1, p1] = futures::Contract<int>();
    auto [f2, p2] = futures::Contract<int>();

    auto join = !futures::Join(std::move(f1), std::move(f2));

    ASSERT_FALSE(join.HasOutput());

    std::move(p1).Set(1);
    ASSERT_FALSE(join.HasOutput());

    std::move(p2).Set(2);
    ASSERT_TRUE(join.HasOutput());

    *std::move(join);
  }

  SIMPLE_TEST(JoinError) {
    using OptInt = std::optional<int>;

    auto [f1, p1] = futures::Contract<OptInt>();
    auto [f2, p2] = futures::Contract<OptInt>();

    auto join = !futures::Join(std::move(f1), std::move(f2));

    ASSERT_FALSE(join.HasOutput());

    std::move(p2).Set(std::nullopt);
    ASSERT_TRUE(join.HasOutput());

    auto output = *std::move(join);
    ASSERT_FALSE(output);
  }

  SIMPLE_TEST(JoinMixedInputs) {
    auto f1 = futures::Invoke([]() {
      return 7;
    });

    auto [f2, p2] = futures::Contract<std::string>();

    auto join = !futures::Join(std::move(f1), std::move(f2));

    ASSERT_FALSE(join.HasOutput());

    std::move(p2).Set("Done");

    ASSERT_TRUE(join.HasOutput());
  }

  SIMPLE_TEST(JoinEagerVector) {
    auto [f1, p1] = futures::Contract<int>();
    auto [f2, p2] = futures::Contract<int>();

    std::vector<futures::EagerFuture<int>> inputs;
    inputs.push_back(std::move(f1));
    inputs.push_back(std::move(f2));

    auto join = !futures::Join(std::move(inputs));

    std::move(p2).Set(2);

    ASSERT_FALSE(join.HasOutput());

    std::move(p1).Set(1);

    ASSERT_TRUE(join.HasOutput());
  }

  SIMPLE_TEST(WithInterrupt) {
    timers::Queue tk;

    {
      // Value
      auto timeout = futures::After(tk.Delay(2s));
      auto value = futures::After(tk.Delay(1s)) | futures::Apply([]() {
        return 42;
      });

      auto with_timeout = futures::Interrupt(std::move(value), std::move(timeout));

      auto output = Await(std::move(with_timeout));

      ASSERT_TRUE(output.has_value());
      ASSERT_EQ(*output, 42);
    }

    {
      // Timeout
      auto timeout = futures::After(tk.Delay(1s));
      auto value = futures::After(tk.Delay(2s)) | futures::Apply([]() {
        return 42;
      });

      auto with_timeout = futures::Interrupt(std::move(value), std::move(timeout));

      auto output = Await(std::move(with_timeout));

      ASSERT_FALSE(output.has_value());
    }

    tk.Stop();
  }

  SIMPLE_TEST(Quorum) {
    std::vector<futures::Promise<int>> promises;
    std::vector<futures::EagerFuture<int>> inputs;

    for (size_t i = 0; i < 5; ++i) {
      auto [f, p] = futures::Contract<int>();
      promises.push_back(std::move(p));
      inputs.push_back(std::move(f));
    }

    auto quorum = !futures::Quorum(std::move(inputs), 3);

    // 1/3
    std::move(promises[2]).Set(17);
    // 2/3
    std::move(promises[4]).Set(42);

    ASSERT_FALSE(quorum.HasOutput());

    // 3/3
    std::move(promises[0]).Set(256);

    ASSERT_TRUE(quorum.HasOutput());

    // > threshold
    std::move(promises[1]).Set(13);

    auto output = *std::move(quorum);

    ASSERT_EQ(output.size(), 3);
    ASSERT_EQ(output[0], 17);
    ASSERT_EQ(output[1], 42);
    ASSERT_EQ(output[2], 256);
  }

  SIMPLE_TEST(MixCombinators) {
    using OptInt = std::optional<int>;

    auto [f1, p1] = futures::Contract<OptInt>();
    auto [f2, p2] = futures::Contract<OptInt>();
    auto [f3, p3] = futures::Contract<OptInt>();

    auto first_of_12 = futures::FirstOf(std::move(f1), std::move(f2));
    auto f = !futures::All(std::move(first_of_12), std::move(f3));

    std::move(p2).Set(std::nullopt);
    std::move(p3).Set(3);

    ASSERT_FALSE(f.HasOutput());

    std::move(p1).Set(1);

    ASSERT_TRUE(f.HasOutput());
  }
}
