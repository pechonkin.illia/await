#include <await/tasks/exe/thread_pool.hpp>
#include <await/tasks/exe/strand.hpp>
#include <await/tasks/boot/submit.hpp>

#include <twist/test/with/wheels/stress.hpp>

#include <twist/test/budget.hpp>

#include <wheels/test/framework.hpp>

#include <thread>

using namespace await::tasks;
using namespace std::chrono_literals;

/////////////////////////////////////////////////////////////////////

class OnePassBarrier {
 public:
  explicit OnePassBarrier(size_t threads) : total_(threads) {
  }

  void PassThrough() {
    arrived_.fetch_add(1);
    while (arrived_.load() < total_) {
      std::this_thread::yield();
    }
  }

 private:
  size_t total_{0};
  std::atomic<size_t> arrived_{0};
};

void ScheduleExecuteRace() {
  ThreadPool pool{1};

  while (twist::test::KeepRunning()) {
    Strand strand(pool);
    OnePassBarrier barrier{2};

    size_t done = 0;

    Submit(strand, [&done, &barrier] {
      ++done;
      barrier.PassThrough();
    });

    barrier.PassThrough();

    Submit(strand, [&done] {
      ++done;
    });

    pool.WaitIdle();

    ASSERT_EQ(done, 2);
  }

  pool.Stop();
}

//////////////////////////////////////////////////////////////////////

TEST_SUITE(StrandMemoryOrderings) {
  TWIST_TEST(ScheduleExecuteRace, 10s) {
    ScheduleExecuteRace();
  }
}

RUN_ALL_TESTS()
