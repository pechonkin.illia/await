#include <twist/rt/run.hpp>

#include <twist/test/assert.hpp>
#include <twist/test/repeat.hpp>

#include <twist/ed/stdlike/thread.hpp>

#include <await/await.hpp>

#include "../event.hpp"

#include <fmt/core.h>

#include <chrono>

using namespace std::chrono_literals;

void StressTest() {
  twist::test::Repeat repeat;

  while (repeat()) {
    int data = 0;
    tutorial::AsyncEvent ready;

    twist::ed::stdlike::thread producer([&] {
      data = 1;
      ready.Fire();
    });

    await::Await(ready.Wait());
    TWIST_ASSERT(data == 1);

    producer.join();
  }

  fmt::println("Iterations: {}", repeat.IterCount());
}

int main() {
  auto params = twist::rt::Params{}.TimeBudget(5s);

  twist::rt::Run(params, [] {
    StressTest();
  });

  return 0;
}