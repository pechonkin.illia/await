#pragma once

#include <await/futures/types/future.hpp>

// Mutual exclusion
#include <twist/ed/stdlike/mutex.hpp>

// Unit type
#include <wheels/core/unit.hpp>

namespace tutorial {

// Asynchronous one-shot event with a single waiter

// Implementation is intentionally suboptimal
// (mutex can be replaced with single atomic & wait-free state machine)
// in favour of simplicity

class AsyncEvent {

  // Future in Await is fundamentally a thunk

  // Thunk is an object representing an (asynchronous) operation
  // that has not been started yet.

  // More on thunks: https://wiki.haskell.org/Thunk

  // Thunk should implement await::futures::lazy::Thunk concept
  // See await/futures/model/thunk.hpp

  // Thunk
  class Waiter {
   public:
    // Declare Future value type
    using ValueType = wheels::Unit;

   private:
    // Type alias for a Future consumer
    using IUnitConsumer = await::futures::IConsumer<wheels::Unit>;

   public:
    explicit Waiter(AsyncEvent* event)
        : event_(event) {
      IAmFuture();
    }

    // Lazy protocol: Start

    // Starts the asynchronous operation (= waiting on AsyncEvent)
    // Initiated by {fibers,threads}::Await algorithm

    // Asynchronous operation should eventually complete the Future by
    // * either producing a result (via IConsumer::Consume)
    // * or propagating cancellation (via IConsumer::Cancel)
    // to the provided consumer

    // IConsumer represents blocked thread, suspended fiber or arbitrary future terminator
    // See await/futures/model/consumer.hpp

    void Start(IUnitConsumer* consumer) {
      consumer_ = consumer;
      event_->Subscribe(/*waiter=*/this);
    }

    // Resume waiter

    void Resume() {
      // Complete future
      consumer_->Consume(wheels::Unit{});
    }

   private:
    static void IAmFuture() {
      // Checks at compile time that Waiter implements SomeFuture (Thunk) concept
      static_assert(await::futures::SomeFuture<Waiter>);
    }

   private:
    AsyncEvent* event_;
    IUnitConsumer* consumer_;
  };

 public:
  // Concrete Future type produced by AsyncEvent::Wait
  using Future = Waiter;

  // Asynchronous
  Future Wait() {
    return Waiter{this};
  }

  // NB: Fire and Subscribe(Waiter*) methods can be invoked concurrently
  // from different threads

  void Fire() {
    Locker locker(mutex_);

    if (waiter_ != nullptr) {
      // Rendezvous
      locker.unlock();
      waiter_->Resume();
    } else {
      fired_ = true;
    }
  }

 private:
  // Drop-in replacement for std::mutex, allows extensive testing
  using Mutex = twist::ed::stdlike::mutex;
  // RAII guard for Mutex
  using Locker = std::unique_lock<Mutex>;

 private:
  // threads::Await(AsyncEvent::Future)
  // -> AsyncEvent::Waiter::Start(thread consumer)
  // -> AsyncEvent::Subscribe(waiter)

  // NB: Fire and Subscribe(Waiter*) methods can be invoked concurrently
  // from different threads

  void Subscribe(Waiter* waiter) {
    Locker locker(mutex_);

    if (fired_) {
      // Rendezvous
      locker.unlock();
      waiter->Resume();
    } else {
      waiter_ = waiter;
    }
  }

 private:
  Mutex mutex_;
  // State guarded by mutex
  bool fired_ = false;
  Waiter* waiter_ = nullptr;
};

}  // namespace tutorial
