#include <twist/rt/run.hpp>
#include <twist/test/repeat.hpp>
#include <twist/test/assert.hpp>

#include <await/executors/impl/pools/compute/thread_pool.hpp>

#include <await/futures/make/submit.hpp>
#include <await/futures/combine/seq/anyway.hpp>
#include <await/futures/syntax/bang.hpp>
#include <await/futures/syntax/go.hpp>

#include <await/await.hpp>

#include "../event.hpp"

#include <fmt/core.h>

#include <chrono>

using namespace await;
using namespace std::chrono_literals;

//////////////////////////////////////////////////////////////////////

void StressTest() {
  executors::pools::compute::ThreadPool pool{5};

  std::atomic<size_t> completed{0};
  std::atomic<size_t> canceled{0};

  twist::test::Repeat repeat;

  while (repeat()) {
    bool data = 0;
    tutorial::AsyncEvent flag;

    auto f = !futures::Submit(pool, [&] {

      auto w = flag.Wait()
               | futures::OnComplete([&](wheels::Unit) {
                   ++completed;
                 })
               | futures::OnCancel([&] {
                   ++canceled;
                 });

      Await(std::move(w));  // <- Consumer

      TWIST_ASSERT(data == 1);
    });

    ~futures::Submit(pool, [&] {
      data = 1;
      flag.Fire();  // <- Producer
    });

    ~futures::Submit(pool, [f = std::move(f)]() mutable {
       std::move(f).RequestCancel();  // <- Interrupter
     });

    pool.WaitIdle();
  }

  pool.Stop();

  size_t iter_count = repeat.IterCount();

  fmt::println("Iterations = {}, completed = {}, canceled = {}",
               iter_count, completed.load(), canceled.load());

  TWIST_ASSERT(iter_count == completed.load() + canceled.load());
}

int main() {
  auto params = twist::rt::Params{}.TimeBudget(5s);

  twist::rt::Run(params, [] {
    StressTest();
  });

  return 0;
}
