#include <await/tasks/exe/fibers/pool.hpp>
#include <await/tasks/exe/fibers/manual.hpp>
#include <await/tasks/exe/thread_pool.hpp>
#include <await/timers/impl/queue.hpp>

#include <await/futures/values/expected.hpp>

#include <await/futures/make/submit.hpp>
#include <await/futures/make/after.hpp>
#include <await/futures/make/value.hpp>
#include <await/futures/make/just.hpp>
#include <await/futures/make/contract.hpp>

#include <await/futures/combine/seq/apply.hpp>
#include <await/futures/combine/seq/monad.hpp>
#include <await/futures/combine/seq/flatten.hpp>
#include <await/futures/combine/seq/anyway.hpp>
#include <await/futures/combine/seq/via.hpp>
#include <await/futures/combine/seq/with.hpp>
#include <await/futures/combine/seq/sched.hpp>
#include <await/futures/combine/seq/timeout.hpp>
#include <await/futures/combine/seq/clone.hpp>
#include <await/futures/combine/seq/prune.hpp>

#include <await/futures/combine/par/first_of.hpp>
#include <await/futures/combine/par/all.hpp>
#include <await/futures/combine/par/interrupt.hpp>

#include <await/futures/run/go.hpp>
#include <await/futures/run/sink.hpp>

#include <await/futures/syntax/bang.hpp>
#include <await/futures/syntax/join.hpp>
#include <await/futures/syntax/or.hpp>
#include <await/futures/syntax/unwrap.hpp>
#include <await/futures/syntax/sequence.hpp>

#include <await/fibers/sched/yield.hpp>

#include <await/await.hpp>

#include <carry/new.hpp>

#include <fmt/core.h>

#include <iostream>
#include <atomic>

using namespace await;

using namespace std::chrono_literals;

//////////////////////////////////////////////////////////////////////

void ExecuteExample() {
  std::cout << "Submit Example" << std::endl;

  tasks::ThreadPool pool{/*threads=*/4};

  {
    // There are no dynamic memory allocations in this scope

    auto task = futures::Submit(pool, [] {
      std::cout << "Hello, World!" << std::endl;
    });

    // Submit task to pool and wait for result
    Await(std::move(task));
  }

  pool.Stop();

  std::cout << std::endl;
}

//////////////////////////////////////////////////////////////////////

void ConceptExample() {
  std::cout << "Concept Example" << std::endl;

  tasks::ThreadPool pool{/*threads=*/4};

  {
    // Annotate `task` type with Future<T> concept
    // https://en.cppreference.com/w/cpp/language/constraints

    futures::Future<int> auto task = futures::Submit(pool, [] {
      return 42;
    });

    // Submit task to pool and wait for result
    Await(std::move(task));
  }

  pool.Stop();

  std::cout << std::endl;
}

//////////////////////////////////////////////////////////////////////

void PipelineExample() {
  std::cout << "Pipeline Example" << std::endl;

  tasks::ThreadPool pool{/*threads=*/4};

  {
    auto pipeline = futures::Value(1)
                    | futures::Via(pool)
                    | futures::Apply([](int value) {
                        return value + 1;
                      })
                    | futures::Apply([](int value) {
                        return value * 2;
                      });

    // Start pipeline and wait for output
    auto value = Await(std::move(pipeline));

    std::cout << "Pipeline -> " << value << std::endl;
  }

  pool.Stop();

  std::cout << std::endl;
}

//////////////////////////////////////////////////////////////////////

void JustViaApplyExample() {
  std::cout << "Just.Via.Apply Example" << std::endl;

  tasks::ThreadPool pool{4};

  // Make new context
  auto ctx = carry::New()
                 .SetString("Example", "Just.Via.Apply")
                 .Done();

  // Set executor and context
  auto task = futures::Just()
              | futures::Via(pool)
              | futures::With(ctx)
              | futures::Apply([] {
                  // Access context of the current task
                  auto ctx = tasks::curr::Context();

                  std::cout << "Example -> " << ctx.GetString("Example") << std::endl;
                });

  Await(std::move(task));

  pool.Stop();

  std::cout << std::endl;
}

//////////////////////////////////////////////////////////////////////

void RecoverExample() {
  std::cout << "Recover Example" << std::endl;

  using Result = tl::expected<int, int>;

  auto f = futures::Invoke([]() -> Result {
             return tl::unexpected(7);
           })
           | futures::AndThen([](int v) -> Result {
               return v + 1;  // Skipped
             })
           | futures::OrElse([](int error) -> Result {
               fmt::println("Error -> {}", error);
               return 42; // Fallback
             });

  auto r = std::move(f) | futures::Unwrap();

  std::cout << "Recover -> "
            << *r << std::endl;

  std::cout << std::endl;
}

//////////////////////////////////////////////////////////////////////

void ContractExample() {
  std::cout << "Contract Example" << std::endl;

  // Contract = (Future, Promise)
  auto [f, p] = futures::Contract<int>();

  {
    // Consumer

    f.IAmEager();

    assert(!f.HasOutput());
  }

  {
    // Producer
    std::move(p).Set(42);
  }

  {
    // Consumer

    assert(f.HasOutput());

    // Unwrap ready result
    // Precondition: f.HasResult() == true
    auto value = std::move(f) | futures::Unwrap();

    std::cout << "Contract -> " << value << std::endl;
  }

  std::cout << std::endl;
}

//////////////////////////////////////////////////////////////////////

void StartExample() {
  std::cout << "Start Example" << std::endl;

  tasks::ThreadPool pool{/*threads=*/4};

  {
    // Create lazy computation
    futures::Future<int> auto l = futures::Submit(pool, [] {
      return 42;
    });

    // Convert lazy Future to eager = start computation
    futures::EagerFuture<int> f = std::move(l) | futures::Start();

    // Consume future asynchronously
    std::move(f)
        | futures::Apply([](int v) {
            std::cout << "Submit -> " << v << std::endl;
          })
        | futures::Go();
  }

  pool.WaitIdle();
  pool.Stop();

  std::cout << std::endl;
}

//////////////////////////////////////////////////////////////////////

void PruneExample() {
  std::cout << "Prune Example" << std::endl;

  struct Widget {
    char buf[256];
  };

  auto f = futures::Just()
           | futures::Apply([w = Widget()] { (void)w; })
           | futures::Prune();

  static_assert(sizeof(f) == sizeof(void*));

  auto g = std::move(f)
           | futures::Apply([] {
               std::cout << "Ok";
             });

  static_assert(sizeof(g) < sizeof(Widget));

  Await(std::move(g));

  std::cout << std::endl;
}

//////////////////////////////////////////////////////////////////////

void BoxExample() {
  std::cout << "Box Example" << std::endl;

  auto l = futures::Value(7)
           | futures::Apply([](int value) {
               return value + 35;
             });

  // Erase thunk type
  futures::BoxedFuture<int> box = std::move(l) | futures::Box();

  int value = *std::move(box);
  std::cout << "Value -> " << value << std::endl;

  std::cout << std::endl;
}

//////////////////////////////////////////////////////////////////////

void AutoBoxingExample() {
  {
    futures::BoxedFuture<int> f = futures::Value(7);
  }
}

//////////////////////////////////////////////////////////////////////

void BangOperatorExample() {
  std::cout << "Bang Example" << std::endl;

  tasks::ThreadPool pool{/*threads=*/4};

  {
    // Create lazy computation
    auto lazy = futures::Submit(pool, [] { return 42; })
                | futures::Apply([](int value) {
                    return value + 1;
                  })
                | futures::Apply([](int value) {
                    std::cout << "Value -> " << value << std::endl;
                  });

    // Bang for strictness!
    // https://ghc.gitlab.haskell.org/ghc/doc/users_guide/exts/strict.html

    futures::EagerUnitFuture eager = !std::move(lazy);

    Await(std::move(eager));
  }

  pool.Stop();

  std::cout << std::endl;
}

//////////////////////////////////////////////////////////////////////

void FirstOfExample() {
  std::cout << "FirstOf Example" << std::endl;

  tasks::fibers::Pool pool{4};

  {
    auto f = futures::Submit(pool,
                             []() -> int {
                               wheels::Defer note([] {
                                 std::cout << "Cancelled" << std::endl;
                               });

                               while (true) {
                                 std::cout << "Running!" << std::endl;
                                 fibers::Yield();
                               }
                             })
             | futures::Anyway([] {
                 std::cout << "Anyway" << std::endl;
               })
             | futures::Start();

    f.IAmEager();

    // Lazy future
    auto l = futures::Submit(pool, [] {
      return 42;
    });

    auto first = futures::FirstOf(std::move(f), std::move(l));

    int value = Await(std::move(first));

    std::cout << "Value -> " << value << std::endl;
  }

  pool.WaitIdle();
  pool.Stop();

  std::cout << std::endl;
}

//////////////////////////////////////////////////////////////////////

void FlattenExample() {
  std::cout << "Flatten Example" << std::endl;

  tasks::fibers::Pool pool{4};

  {
    futures::Future<int> auto f =
        futures::Submit(pool,
                        [] {
                          // Nested eager asynchronous computation
                          return !futures::Spawn([] {
                            for (size_t i = 0; i < 7; ++i) {
                              fibers::Yield();
                            }
                            return 42;
                          });
                        })
        | futures::Flatten();

    int value = Await(std::move(f));

    std::cout << "Value -> " << value << std::endl;
  }

  pool.WaitIdle();
  pool.Stop();

  std::cout << std::endl;
}

//////////////////////////////////////////////////////////////////////

void YieldExample() {
  std::cout << "Yield Example" << std::endl;

  tasks::ThreadPool pool{1};

  {
    futures::Submit(pool,
                    [] {
                      std::cout << "1" << std::endl;
                    })
        | futures::Apply([] {
            // Submit "interrupt" task
            futures::Spawn([] {
              std::cout << "Interrupt" << std::endl;
            }) | futures::Go();
            std::cout << "2" << std::endl;
          })
        | futures::Apply([] {
            std::cout << "3" << std::endl;
          })
        | futures::Apply([&] {
            std::cout << "4" << std::endl;
          })
        | futures::Yield()  // <-- Yield to "interrupt" task
        | futures::Apply([] {
            std::cout << "5" << std::endl;
          })
        | futures::Go();
  }

  pool.WaitIdle();
  pool.Stop();

  std::cout << std::endl;
}

//////////////////////////////////////////////////////////////////////

void CloneExample() {
  std::cout << "Clone Example" << std::endl;

  tasks::ThreadPool pool{4};

  {
    auto exe = futures::Submit(pool, [] {
      return "Hi";
    });

    auto [left, right] = std::move(exe) | futures::Clone();

    std::cout << "Right -> " << Await(std::move(right)) << std::endl;

    {
      auto result = *std::move(left);
      std::cout << "Left -> " << *result << std::endl;
    }
  }

  pool.Stop();

  std::cout << std::endl;
}

//////////////////////////////////////////////////////////////////////

void AfterExample() {
  std::cout << "After Example" << std::endl;

  timers::Queue timers;

  // Future that will be fulfilled with Unit value after 1s
  auto after1s = futures::After(timers.Delay(1s));

  auto print_later = std::move(after1s) | futures::Apply([] {
                       std::cout << "1 second later..." << std::endl;
                     });

  Await(std::move(print_later));

  timers.Stop();

  std::cout << std::endl;
}

//////////////////////////////////////////////////////////////////////

void WithTimeoutExample() {
  std::cout << "WithTimeout Example" << std::endl;

  tasks::fibers::Pool pool{4};
  timers::Queue timers;

  std::atomic<int> iters = 0;

  // Infinite loop
  auto loop = futures::Submit(pool, [&iters] {
    while (true) {
      ++iters;
      fibers::Yield();
    }
    return 7;
  });

  auto with_timeout = std::move(loop) | futures::WithTimeout(timers.Delay(1s));

  auto o = Await(std::move(with_timeout));
  assert(!o.has_value());

  std::cout << "Iterations made: " << iters.load() << std::endl;

  timers.Stop();
  pool.Stop();

  std::cout << std::endl;
}

//////////////////////////////////////////////////////////////////////

void SinkExample() {
  std::cout << "Sink Example" << std::endl;

  tasks::fibers::ManualExecutor manual;

  auto fiber = futures::Submit(manual,
                               [] {
                                 for (size_t i = 0; i < 7; ++i) {
                                   fibers::Yield();
                                 }
                               })
               | futures::Sink();

  // Submit task to manual executor
  fiber.Start();

  size_t steps = manual.Drain();

  std::cout << "Steps = " << steps << std::endl;
  std::cout << std::endl;
}

//////////////////////////////////////////////////////////////////////

void SyntaxExample() {
  std::cout << "Syntax (Operators) Example" << std::endl;

  tasks::fibers::Pool pool{4};
  timers::Queue timers;

  {
    // Pipe operator: |
    auto f = futures::After(timers.Delay(2s))
             | futures::Apply([] {
                 return 7;
               });

    auto g = futures::After(timers.Delay(1s))
             | futures::Apply([] {
                 return 11;
               });

    // Or operator: or (||)
    auto first = std::move(f) or std::move(g);

    auto value = Await(std::move(first));

    std::cout << value << std::endl;
  }

  {
    auto exe = futures::Submit(pool, [] {
      std::cout << "Compute" << std::endl;
      return 17;
    });

    auto after = futures::After(timers.Delay(2s));

    // Join operator: and (&&)
    auto join = std::move(exe) and std::move(after);

    Await(std::move(join));
  }

  {
    // Sequence operator: >>
    auto f = futures::After(timers.Delay(1s))
            >> futures::After(timers.Delay(1s))
            >> futures::Invoke([] {
                 std::cout << "2s later" << std::endl;
               });

    Await(std::move(f));
  }

  timers.Stop();
  pool.Stop();

  std::cout << std::endl;
}

//////////////////////////////////////////////////////////////////////

int main() {
  ExecuteExample();
  ConceptExample();
  PipelineExample();
  JustViaApplyExample();
  RecoverExample();
  ContractExample();
  StartExample();
  PruneExample();
  BoxExample();
  AutoBoxingExample();
  BangOperatorExample();
  FirstOfExample();
  FlattenExample();
  YieldExample();
  CloneExample();
  AfterExample();
  WithTimeoutExample();
  SinkExample();
  SyntaxExample();

  return 0;
}
