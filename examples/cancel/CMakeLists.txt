set(EXAMPLE_TARGET await_example_cancel)

add_executable(${EXAMPLE_TARGET} main.cpp)
target_link_libraries(${EXAMPLE_TARGET} await)
